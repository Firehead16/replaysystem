﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System.IO;

public class CharMovement : MonoBehaviour
{
    [SerializeField]
    private InputActionAsset controls;
    [SerializeField]
    private float speed;
    [SerializeField]
    public bool IsReplay, RecordReplay;

    public struct Action
    {
        public float CurrentTime;
        public Vector2 Vec2;
    }

    [SerializeField]
    public List<Action> ActionList = new List<Action>();

    void OnEnable() => controls.Enable();
    void OnDisable() => controls.Disable();

    void Start() {
        if(File.Exists("replay.txt") && IsReplay){
            string[] getReplay = File.ReadAllLines("replay.txt");
            foreach (var str in getReplay)
            {
                ActionList.Add(new Action{CurrentTime = float.Parse(str.Split()[0]), Vec2 = 
                new Vector2(float.Parse(str.Split()[1]), float.Parse(str.Split()[2]))});
            }
        }
    }

    void FixedUpdate()
    {
        Vector2 move = controls.FindAction("Move").ReadValue<Vector2>();
        if (move != Vector2.zero && RecordReplay)
        {
            ActionList.Add(new Action { CurrentTime = Time.timeSinceLevelLoad, Vec2 = move });
        }
        Vector3 trp = transform.position;
        transform.position = new Vector3(trp.x + move.x * Time.deltaTime * speed, trp.y, trp.z + move.y * Time.deltaTime * speed);

        if(IsReplay){
            if(ActionList[0].CurrentTime <= Time.timeSinceLevelLoad && ActionList.Count > 0){
                transform.position = new Vector3(trp.x + ActionList[0].Vec2.x * Time.deltaTime * speed,
                 trp.y, trp.z + ActionList[0].Vec2.y * Time.deltaTime * speed);
                 ActionList.RemoveAt(0);
            }
            else Debug.Log(ActionList[0].CurrentTime + " in file " + Time.timeSinceLevelLoad + " irl");
        }
    }

    void OnApplicationQuit()
    {
        if(RecordReplay) {
            List<string> replay = new List<string>();
            foreach (var item in ActionList)
            {
                replay.Add(item.CurrentTime + " " + item.Vec2.x + " " + item.Vec2.y);
            }
    
            File.WriteAllLines("replay.txt", replay);
        }
    }
}
